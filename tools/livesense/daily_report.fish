set ROOT_DIR $HOME/Documents/inokuchi/daily_report
set day ( date '+%y%m%d' )

set f "$ROOT_DIR/$day.md"
set template "$ROOT_DIR/template.md"

if test -e $f
  vim $f
else
  cp $template $f
  set title '# 日報' ( date '+%y/%m/%d' )
  gsed -i "1i $title" $f
  vim $f
end
