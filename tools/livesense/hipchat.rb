require 'pp'
require 'uri'
require 'json'
require 'rest-client'
require 'time'

class HipChat
  @@GREETING_START = "おはようございます！"
  @@GREETING_END   = "お疲れ様です！"

  def good_morning
    HipChatHelper.sendRoom("TK - 挨拶", @@GREETING_START)
    HipChatHelper.sendMe(@@GREETING_START)
  end

  def good_bye
    HipChatHelper.sendRoom("TK - 挨拶", @@GREETING_END)
    HipChatHelper.sendMe(@@GREETING_END)
  end

  def starttime
    HipChatHelper.getTime(@@GREETING_START)
  end

  def endtime
    HipChatHelper.getTime(@@GREETING_END)
  end

end

class HipChatHelper

  @@api_base = "https://api.hipchat.com/v2"
  @@token = {
    view_messages: "V2FTswO1IETBWfOISNObIIQsJeE740glUTYIRGI1",
    view_group:    "iS1tIN1Vca4ugbIiDO1VkaA0bxCEdq9fULSl1l9F",
    view_room:     "jUgHimfzEFDO7tNgfsHJ0PadUQHqrUHqO3tvd3V7",
    send_message:  "D7J1bUL6pRvUoWVYmuD8MxT0agKuhBltj8rXphMd"
  }

  def self.api_url(path, token_scope)
    token = @@token[token_scope]
    url_raw =  @@api_base + path + "?auth_token=#{token}"
    return URI.escape(url_raw)
  end

  def self.all_room
    result = RestClient.get api_url("/room", :view_room), max_results: 1000
    pp JSON.parse(result.body)
  end

  def self.room(name)
    result = RestClient.get api_url("/room/#{name}", :view_room), max_results: 1000
    return JSON.parse(result.body)
  end

  def self.send(url, msg)
    pp url
    RestClient.post url, msg, content_type: 'text/plain;charset=UTF-8'
  end

  def self.sendRoom(room_name, msg)
    url = api_url("/room/#{room(room_name)["id"]}/message", :send_message)
    send url, msg
  end

  def self.sendPrivate(user_id, msg)
    url = api_url("/user/#{user_id}/message", :send_message)
    send url, msg
  end

  def self.sendMe(msg)
    sendPrivate("@inokuchi", msg)
  end

  def self.getMine(key)
    url = api_url("/user/@inokuchi/history/latest", :view_messages)
    ret = JSON.parse(RestClient.get url,  max_results: 1000)
    return ret["items"].select { |item| item["message"] == key }
  end

  def self.getTime(key)
    result = getMine(key).select { |item| today?(item["date"]) }
    pp Time.parse(result.first['date']).localtime.strftime("%H:%M")
  end

  def self.alreadySend?(room_name, msg)
    result = RestClient.get api_url("/room/#{room_name}/history", :view_messages), params: { max_results: 1000, end_date: "20171031", timezone: 'JP' }
    return JSON.parse(result.body)
  end


  def self.today?(timeStr)
    time = Time.parse(timeStr)
    return time.localtime.strftime("%y%m%d") == Time.now.strftime("%y%m%d")
  end

end
