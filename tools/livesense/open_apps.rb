def open_browser(urls)
  browser = '/Applications/Google\ Chrome.app'
  cmd = "open -a #{browser}"
  urls.each do |url|
    cmd += " #{url}"
  end
  system cmd
end

def open_apps(apps)
  apps.each do |app|
    system "open -a #{app}"
  end
end
