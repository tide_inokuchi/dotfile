require_relative './open_apps.rb'
require_relative './hipchat.rb'

weblist = {
  todoist: 'https://todoist.com/app?lang=en#project%2F2166517038',
  toggl:   'https://toggl.com/app/timer',
  cybozu:  'https://cybozu.ad.livesense.co.jp/scripts/cbgrn/grn.exe/portal/index'
}

applist = [
  'HipChat',
  'Franz',
]

open_browser(weblist.values)
open_apps(applist)

HipChat.new.good_morning
