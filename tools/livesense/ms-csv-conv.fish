function __ms_csv_conv_usage
  echo "Specify a MS F**king csv"
  echo 'ex) $ ms-csv-conv path/to/source/csv output/path'
end

if not test (count $argv) = 1; and not test (count $argv) = 2
  __ms_csv_conv_usage
  exit
end

# get source filename
set src_csv $argv[1]

if test (count $argv) = 1
  set dest_dir (dirname $argv[1])
  set output (string join _ pretty (basename $argv[1]))
else if test (count $argv) = 2
  set dest_dir (dirname $argv[2])
  set output (basename $argv[2])

  if not test -e $dest_dir
    mkdir -p $dest_dir
  end
end


# convert
# SJIS -> UTF8
iconv -f SJIS -t UTF8 $src_csv > $dest_dir/$output
# CR -> LF
gsed -i -e 's/\r/\n/g' $dest_dir/$output
