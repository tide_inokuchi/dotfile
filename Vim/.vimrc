" -----------------------------------------
"       _      _             _
"    __| | ___(_)_ __ __   _(_)_ __ ___
"   / _` |/ _ \ | '_ \\ \ / / | '_ ` _ \
"  | (_| |  __/ | | | |\ V /| | | | | | |
"   \__,_|\___|_|_| |_(_)_/ |_|_| |_| |_|
"
" -----------------------------------------

" dein scripts {{{
if &compatible
  set nocompatible " Be iMproved
endif

set runtimepath^=$HOME/.vim/dein/repos/github.com/Shougo/dein.vim

let s:home_dir = expand('$HOME')
let s:vim_root_dir = s:home_dir . '/.vim'
let s:dein_dir = s:vim_root_dir . '/dein'
let s:dein_toml = s:vim_root_dir . '/dein.toml'
let s:lazy_toml = s:vim_root_dir . '/lazy.toml'
if dein#load_state(s:dein_dir)
  call dein#begin(s:dein_dir)
  call dein#load_toml(s:dein_toml, {'lazy': 0})
  call dein#load_toml(s:lazy_toml, {'lazy': 1})
  " Required:
  call dein#end()
  call dein#save_state()
endif
" }}}


" 画面表示の設定
syntax enable
set background=dark
autocmd ColorScheme * highlight LineNr ctermfg=247
colorscheme hybrid
set title          " Window上部にファイル名を表示
set number         " 行番号を表示する
set ruler          " カーソル位置の行数と列数を表示
set laststatus=2   " ステータス行を常に表示
set showcmd        " 入力中のコマンドを右下に表示
set cmdheight=2    " メッセージ表示欄を2行確保
set showmatch      " 対応する括弧を強調表示
set helpheight=999 " ヘルプを画面いっぱいに開く
set list           " 不可視文字を表示
" 不可視文字の表示記号指定
set listchars=tab:>>,trail:_,eol:$,extends:>,precedes:<,nbsp:%
set ambiwidth=double " 全角記号の文字幅を修正するオプション
set encoding=utf-8 " デフォルトの文字エンコードをutf-8に指定
set fenc=utf-8

" 全角スペースの表示
" http://inari.hatenablog.com/entry/2014/05/05/231307
" {{{
function! ZenkakuSpace()
    highlight ZenkakuSpace cterm=underline ctermfg=lightblue guibg=darkgray
endfunction

if has('syntax')
  augroup ZenkakuSpace
    autocmd!
    autocmd ColorScheme * call ZenkakuSpace()
    autocmd VimEnter,WinEnter,BufRead * let w:m1=matchadd('ZenkakuSpace', '　')
  augroup END
  call ZenkakuSpace()
endif
" }}}

" カーソル移動関連の設定 {{{
set backspace=indent,eol,start " Backspaceキーの影響範囲に制限を設けない
set whichwrap=b,s,h,l,<,>,[,]  " 行頭行末の左右移動で行をまたぐ
set scrolloff=8                " 上下8行の視界を確保
set sidescrolloff=16           " 左右スクロール時の視界を確保
set sidescroll=1               " 左右スクロールは一文字づつ行う
" }}}

" ファイル処理関連の設定
set autoread   "外部でファイルに変更がされた場合は読みなおす
set nobackup   " ファイル保存時にバックアップファイルを作らない
set noswapfile " ファイル編集中にスワップファイルを作らない
set hidden     " 保存されていないファイルが有るときでも別のファイルを開くことができる


" 検索/置換の設定
set hlsearch   " 検索文字列をハイライトする
" ESC2回押しで検索後のハイライトを消す
nmap <silent> <Esc><Esc> :nohl<CR><Esc>

set incsearch  " インクリメンタルサーチを行う
set ignorecase " 大文字と小文字を区別しない
set smartcase  " 大文字と小文字が混在した言葉で検索を行った場合に限り、大文字と小文字を区別する
set wrapscan   " 最後尾まで検索を終えたら次の検索で先頭に移る
set gdefault   " 置換の時 g オプションをデフォルトで有効にする

" タブ/インデントの設定
set expandtab     " タブ入力を複数の空白入力に置き換える
set tabstop=2     " 画面上でタブ文字が占める幅
set shiftwidth=2  " 自動インデントでずれる幅
set softtabstop=2 " 連続した空白に対してタブキーやバックスペースキーでカーソルが動く幅
set autoindent    " 改行時に前の行のインデントを継続する
set smartindent   " 改行時に入力された行の末尾に合わせて次の行のインデントを増減する

" OSのクリップボードをレジスタ指定無しで Yank, Put 出来るようにする
set clipboard=unnamed,unnamedplus
"set paste

" マウスの入力を受け付ける
set mouse=a

" Windows でもパスの区切り文字を / にする
set shellslash

" コマンドラインモードでTABキーによるファイル名補完を有効にする
set wildmenu wildmode=list:longest,full

" コマンドラインの履歴を10000件保存する
set history=10000

"ビープ音すべてを無効にする
set visualbell t_vb=
set noerrorbells "エラーメッセージの表示時にビープを鳴らさない

" for indent?
filetype on
filetype plugin on
filetype indent on

" .mdのファイルもfiletypeがmarkdownとなるようにする
au BufNewFile,BufRead *.md set filetype=markdown
" .slimファイル"
au BufNewFile,BufRead *.slim setlocal filetype=slim
" .ejsファイル"
au BufNewFile,BufRead *.ejs setlocal filetype=ejs
" .tsファイル"
au BufNewFile,BufRead *.ts setlocal filetype=typescript

" ビジュアルモードで、# で選択した文字列を検索する
vnoremap # "zy:let @/ = @z<CR>n

" カレントディレクトリを自動的に移動
set autochdir

" for macvim {{{
if has('gui_macvim')
  set showtabline=2    " タブを常に表示
  set transparency=5   " 透明度
  set imdisable        " IME OFF
  set guioptions-=T    " ツールバー非表示
  set antialias        " アンチエイリアス
  set nomacatsui
  set termencoding=japan
  set visualbell t_vb= " ビープ音なし

  " ビジュアル選択(D&D他)を自動的にクリップボードへ (:help guioptions_a)
  set guioptions+=a
endif
" }}}
