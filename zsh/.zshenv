# export PATH=$HOME/.nvm/versions/node/v6.2.2/bin/npm:/usr/local/bin:/usr/local/sbin:$PATH
export PATH="${HOME}/.scalaenv/bin:${PATH}"
export PATH=~/Library/Android/sdk/platform-tools:$PATH
# export NODE_PATH=$HOME/.nvm/versions/node/v6.2.2/lib/node_modules/
export PATH="$HOME/.pyenv/shims:$PATH"
# source $HOME/.nvm/nvm.sh
