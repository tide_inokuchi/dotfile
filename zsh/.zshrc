autoload -U promptinit && promptinit
prompt pure

autoload -U compinit
compinit -u

export ZPLUG_HOME=/usr/local/opt/zplug
source $ZPLUG_HOME/init.zsh

# 補完時に大文字/小文字を区別しない
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# PROMPT="%H%# "
# RPROMPT="%*"
# [hostname]%

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth:erasedups
HISTIGNORE="fg*:bg*:history*:cd*:ls*:la*"
HISTTIMEFORMAT='%Y%m%d %T  '
HISTFILE=~/.zsh_history
HISTSIZE=1000000
SAVEHIST=1000000
setopt hist_ignore_dups     # ignore duplication command history list
setopt share_history        # share command history data


if [[ -x 'which colordiff' ]]; then
  alias diff='colordiff -u'
else
  alias diff='diff -u'
fi
alias vi="vim"
alias df='df -h'
alias ls='gls --color=auto'
alias ll='gls -la --color=auto'
alias la='gls -la --color=auto'
alias al='gls -la --color=auto'
alias mv='gmv'
alias grep='grep -n --color=auto'
alias hw='hw -e'
alias jagapi='ssh jagapi'
alias trcn='ftp ftp://catfood.jp-tarachine:ssk003loliftphif@users036.lolipop.jp/www/'
alias brew-update='brew update && brew upgrade -y && brew cleanup && brew cask cleanup'
alias cdd='cd $HOME/dev'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

alias gs='git s'
alias gfe='git fetch'
alias gfea='git fetch --all'
alias gco='git co'
alias gbr='git br'
alias gbra='git bra'
alias gad='git add'
alias gcm='git commit -m'
alias gcma='git commit -a'

eval "$(rbenv init -)"
eval "$(scalaenv init -)"

zplug "b4b4r07/enhancd", use:enhancd.sh
# compinit 以降に読み込むようにロードの優先度を変更する（10~19にすれば良い）
zplug "zsh-users/zsh-syntax-highlighting", defer:2
zplug "zsh-users/zsh-completions"

function peco-history-selection() {
  BUFFER=`history -n 1 | tail -r  | awk '!a[$0]++' | peco`
  CURSOR=$#BUFFER
  zle reset-prompt
}
zle -N peco-history-selection
bindkey '^R' peco-history-selection


#############################################################
# _____ __  __ _   ___  __
# |_   _|  \/  | | | \ \/ /
#   | | | |\/| | | | |\  /
#   | | | |  | | |_| |/  \ 
#   |_| |_|  |_|\___//_/\_\ 
#
function is_exists() { type "$1" >/dev/null 2>&1; return $?; }
function is_osx() { [[ $OSTYPE == darwin* ]]; }
function is_screen_running() { [ ! -z "$STY" ]; }
function is_tmux_runnning() { [ ! -z "$TMUX" ]; }
function is_screen_or_tmux_running() { is_screen_running || is_tmux_runnning; }
function shell_has_started_interactively() { [ ! -z "$PS1" ]; }
function is_ssh_running() { [ ! -z "$SSH_CONECTION" ]; }

function tmux_automatically_attach_session()
{
    if is_screen_or_tmux_running; then
        ! is_exists 'tmux' && return 1

        if is_tmux_runnning; then
            echo "${fg_bold[red]} _____ __  __ _   ___  __ ${reset_color}"
            echo "${fg_bold[red]}|_   _|  \/  | | | \ \/ / ${reset_color}"
            echo "${fg_bold[red]}  | | | |\/| | | | |\  /  ${reset_color}"
            echo "${fg_bold[red]}  | | | |  | | |_| |/  \  ${reset_color}"
            echo "${fg_bold[red]}  |_| |_|  |_|\___//_/\_\ ${reset_color}"
        elif is_screen_running; then
            echo "This is on screen."
        fi
    else
        if shell_has_started_interactively && ! is_ssh_running; then
            if ! is_exists 'tmux'; then
                echo 'Error: tmux command not found' 2>&1
                return 1
            fi

            if tmux has-session >/dev/null 2>&1 && tmux list-sessions | grep -qE '.*]$'; then
                # detached session exists
                tmux list-sessions
                echo -n "Tmux: attach? (y/N/num) "
                read
                if [[ "$REPLY" =~ ^[Yy]$ ]] || [[ "$REPLY" == '' ]]; then
                    tmux attach-session
                    if [ $? -eq 0 ]; then
                        echo "$(tmux -V) attached session"
                        return 0
                    fi
                elif [[ "$REPLY" =~ ^[0-9]+$ ]]; then
                    tmux attach -t "$REPLY"
                    if [ $? -eq 0 ]; then
                        echo "$(tmux -V) attached session"
                        return 0
                    fi
                fi
            fi

            if is_osx && is_exists 'reattach-to-user-namespace'; then
                # on OS X force tmux's default command
                # to spawn a shell in the user's namespace
                tmux_config=$(cat $HOME/.tmux.conf <(echo 'set-option -g default-command "reattach-to-user-namespace -l $SHELL"'))
                tmux -f <(echo "$tmux_config") new-session && echo "$(tmux -V) created new session supported OS X"
            else
                tmux new-session && echo "tmux created new session"
            fi
        fi
    fi
}
tmux_automatically_attach_session
################################### tmux setting end ##
