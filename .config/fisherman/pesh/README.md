# pesh

pesh : PEco Select History

## Install

With [fisherman]

```
fisher inokuc/pesh
```

## Usage

Add below to `config.fish` :+1:

```fish
function fish_user_key_bindings
  bind \cr pesh # Bind for peco select history to Ctrl+r
end
```

[fisherman]: https://github.com/fisherman/fisherman
