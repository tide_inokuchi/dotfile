function pesh -d "PEco Select History"
  if test (count $argv) = 0
    set peco_flags --initial-filter Fuzzy
  else
    set peco_flags --query "$argv"
  end

  history|peco $peco_flags|read foo

  if [ $foo ]
    commandline $foo
  else
    commandline ""
  end
end
