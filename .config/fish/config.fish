alias daily-report 'fish ~/dotfile/tools/livesense/daily_report.fish'
alias ms-csv-conv  'fish ~/dotfile/tools/livesense/ms-csv-conv.fish'

# Alias
# if [[ -x 'which colordiff' ]]
#   alias diff 'colordiff -u'
# else
#   alias diff 'diff -u'
# fi
alias vi 'vim'
alias df 'df -h'
alias ls 'gls --color=auto'
alias ll 'gls -la --color=auto'
alias la 'gls -la --color=auto'
alias al 'gls -la --color=auto'
alias mv 'gmv'
alias grep 'grep -n --color=auto'
alias hw 'hw -e -n'
alias jagapi 'ssh jagapi'
alias trcn 'ftp ftp://catfood.jp-tarachine:ssk003loliftphif@users036.lolipop.jp/www/'
alias brew-update 'brew update; and brew upgrade -y; and brew cleanup; and brew cask cleanup'
alias cdd 'cd $HOME/dev'
alias ..    'cd ..'
alias ...   'cd ../..'
alias ....  'cd ../../..'
alias ..... 'cd ../../../..'

alias tree 'tree -N'

# Git
alias gs   'git status'
alias gfe  'git fetch'
alias gfa  'git fetch --all'
alias gco  'git checkout'
alias gcma 'git commit -am'
alias gbr  'git branch'
alias gbra 'git branch -a'
alias gdf  'git diff --color-words'
alias gl   'git log --graph --decorate --oneline --date=iso'
alias gemp 'git commit --allow-empty -m'

# Open Apps (for Mac)
if test (uname) = 'Darwin'
  # markdown (.md)
  alias md 'open -a Marked\ 2'
end


# anyenv settings {{{
if test (type -q anyenv)
	if test -d (anyenv root)
		anyenv init - | source
	end
end
# }}}

# goenv settings {{{
# if test -d (goenv root)
#   set -x GOPATH $HOME/.go
#   set -x PATH $PATH $GOPATH/bin
# end
# }}}

# Key Binding
function fish_user_key_bindings
  bind \cr pesh # Bind for peco select history to Ctrl+r
  bind \cx\cr peco_recentd
end
set --export PATH  "$HOME/.anyenv/bin" $PATH
set --export ANDROID_HOME "$HOME/Library/Android/sdk"
