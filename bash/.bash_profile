export PATH=/Users/inokuchis/.nvm/versions/node/v6.2.2/bin/npm:/usr/local/bin:/usr/local/sbin:$PATH
export PS1="\[\033[33m\]\n\u : \w\n\[\033[0m\] => \$ "

export NODE_PATH=/Users/inokuchis/.nvm/versions/node/v6.2.2/lib/node_modules/
source ~/.nvm/nvm.sh

# gitのtab補完を有効化
source /usr/local/etc/bash_completion.d/git-prompt.sh
source /usr/local/etc/bash_completion.d/git-completion.bash
source /usr/local/etc/bash_completion.d/tig-completion.bash

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth:erasedups
HISTIGNORE="fg*:bg*:history*:cd*:ls*:la*"
HISTTIMEFORMAT='%Y%m%d %T  '

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=2000

# some more ls aliases
if [[ -x 'which colordiff' ]]; then
  alias diff='colordiff -u'
else
  alias diff='diff -u'
fi
alias df='df -h'
alias ls='gls --color=auto'
alias la='gls -la --color=auto'
alias al='gls -la --color=auto'
alias mv='gmv'
alias grep='grep -n --color=auto'
alias hw='hw -e'
alias jagapi='ssh jagapi'
alias trcn='ftp ftp://catfood.jp-tarachine:ssk003loliftphif@users036.lolipop.jp/www/'
alias brew-update='brew update && brew upgrade -y && brew cleanup && brew cask update && brew cask cleanup'
alias cdev='cd ~/dev'
alias cdl='cd ~/dev/letronc/'

eval "$(rbenv init -)"
